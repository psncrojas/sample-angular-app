package main

name = input.metadata.name

required_deployment_labels {
        input.metadata.labels["app"]
}

deny[msg] {
  input.kind = "Deployment"
  not required_deployment_labels
  msg = sprintf("%s must include Kubernetes recommended labels: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/#labels", [name])
}

deny[msg] {
  input.kind = "Deployment"
  not input.spec.selector.matchLabels.app
  msg = "Containers must provide app label for pod selectors"
}
